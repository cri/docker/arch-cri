FROM archlinux:base-devel

ENV LC_ALL en_US.UTF-8
ENV LANG   en_US.UTF-8

RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen \
    && locale-gen

RUN yes | pacman -Syu systemd

RUN pacman -Sy --noconfirm --needed archlinux-keyring \
    && pacman -Syu --noconfirm --needed aws-cli git

RUN mkdir /app \
    && groupadd -g 10001 app \
    && useradd -r -d /app -u 10001 -g app app \
    && chown app:app /app

WORKDIR /app
